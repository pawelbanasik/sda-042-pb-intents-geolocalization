package com.example.rent.sda_042_mw_intents;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String SEPARATOR = ",";
    public static final String SCHEMA = "geo:";
    public static final String SCHEMA_EXTRA = "0,0?q=";

    @BindView(R.id.edit_text_latitude)
    protected EditText editTextLatitude;

    @BindView(R.id.edit_text_longitude)
    protected EditText editTextLongitude;

    @BindView(R.id.edit_text_label)
    protected EditText editTextLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_send_coordinates)
    protected void sendCoordinates() {

        String latitude = editTextLatitude.getText().toString();
        String longitude = editTextLongitude.getText().toString();
        String label = "(" + editTextLabel.getText().toString() + ")";
        String sum = SCHEMA + SCHEMA_EXTRA + latitude + SEPARATOR + longitude + label;

        Uri geoLocation = Uri.parse(sum);

        Intent intent = new Intent(Intent.ACTION_VIEW, geoLocation);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);

        }

    }
}
